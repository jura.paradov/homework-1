# Remove equal adjacent elements
#
# Example input: [1, 2, 2, 3]
# Example output: [1, 2, 3]
def remove_adjacent(lst):
    if not lst:
        return lst
    new_lst = []
    for i in range(len(lst)-1):
        if lst[i] != lst[i+1]:
            new_lst.append(lst[i])
    new_lst.append(lst[-1])
    return new_lst

# Merge two sorted lists in one sorted list in linear time
#
# Example input: [2, 4, 6], [1, 3, 5]
# Example output: [1, 2, 3, 4, 5, 6]
def linear_merge(lst1, lst2):
    new_lst = []
    ptr1 = 0
    ptr2 = 0
    size1 = len(lst1)
    size2 = len(lst2)
    while ptr1 != size1 and ptr2 != size2:
        if lst1[ptr1] < lst2[ptr2]:
            new_lst.append(lst1[ptr1])
            ptr1 += 1
        else:
            new_lst.append(lst2[ptr2])
            ptr2 += 1
    return new_lst + lst1[ptr1:] + lst2[ptr2:]